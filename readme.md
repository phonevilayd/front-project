Project front P16 Café

Ceci est le site d'un salon de thé/bar à chats qui contient toutes les informations, le contenu de celui-ci ainsi que la possibilité d'une réservation en ligne.

Le site contient 6 pages :

- index : Page d'accueil du site;
- index2 : Page contenant les résidents & résidentes;
- index3 : Menu & consommations;
- index4 : Formulaire de réservation (les champs sont fonctionnels, sauf celui de la date); 
- index5 : Informations sur l'établissement & map.

Les maquettes ont été crées à l'aide de Figma.

